# Conda templates

## Base

The Conda template file `conda/base.yml` can be included via

```yaml
include:
  - project: computing/gitlab-ci-templates
    file: conda/base.yml
```

This file provides the following job templates:

### `.conda:base`

The base Conda job template that just configures some conda variables
and sets up default caching of the `.cache` directory.

Example usage:

```yaml
test:
  extends:
    - .conda:base
  script:
    - conda create -n test python
    - conda activate test
    - conda list
    - python -c "print('Hello world')"
```

Conda variables:

| Name              | Default                               | Purpose                                  |
| ----------------- | ------------------------------------- | ---------------------------------------- |
| `CONDARC`         | `${CI_PROJECT_DIR}/.condarc`          | where to store the conda configuration   |
| `CONDA_BLD_PATH`  | `${CI_PROJECT_DIR}/conda-bld`         | where to write conda-build outputs       |
| `CONDA_ENVS_PATH` | `${CI_PROJECT_DIR}/envs`              | where to create conda environments ild`  |
| `CONDA_PKGS_DIRS` | `${CI_PROJECT_DIR}/.cache/conda/pkgs` | where to cache downloaded conda packages |

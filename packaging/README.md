# Packaging templates

## RHEL

The RHEL template files `packaging/rhel.yml` can be included via

```yaml
include:
  - project: computing/gitlab-ci-templates
    file: packaging/rhel.yml
```

This file provides the following job templates

### `.rhel:base`

This is a basic template to configure sensible default variables and
caching in order to cache YUM operations between jobs.

This template does not provide any `script` content.

Example usage:

```yaml
rhel-test:
  extends:
    - .rhel:base
  before_script:
    - yum ${YUM_OPTIONS} install python3
  script:
    - python3 --version
```

### `.rhel:srpm`

This job takes in a `${TARBALL}` and returns a source RPM as an artifact.

Example usage:

```yaml
srpm:
  extends:
    - .rhel:srpm
  variables:
    TARBALL: myproject.tar.gz
```

Input Variables:

| Name | Default | Purpose |
| ---- | ------- | ------- |
| `EPEL` | `false` | if `true`, configure EPEL package repo and install `epel-rpm-macros` |
| `POWERTOOLS` | `false` | if `true`, enable the CentOS 8 PowerTools repo module |
| `SRPM_DEPENDENCIES` | `""` (empty) | extra packages to `yum install` before building the source RPM |
| `TARBALL` | `${CI_PROJECT_NAME}-*.tar.*` | source tarball |

### `.rhel:rpm`

This job takes in a source RPM and returns one or more binary RPMS as artifacts.

Example usage:

```yaml
rpm:
  extends:
    - .rhel:rpm
  needs:
    - srpm
  variables:
    SRPM: "myproject-*.src.rpm"
```

Input Variables:

| Name | Default | Purpose |
| ---- | ------- | ------- |
| `EPEL` | `false` | if `true`, configure EPEL package repo and install `epel-rpm-macros` |
| `POWERTOOLS` | `false` | if `true`, enable the CentOS 8 PowerTools repo module |
| `RPM_DEPENDENCIES` | `""` (empty) | extra packages to `yum install` before building the binary RPMs |
| `SRPM` | `${CI_PROJECT_NAME}-*.src.rpm` | source RPM |

### `.rhel:lint`

Runs `rpmlint` against RPMs pulled in from dependencies

Example usage:

```yaml
rpmlint:
  extends:
    - .rhel:lint
  needs:
    - rpm
```

Input Variables:

| Name | Default | Purpose |
| ---- | ------- | ------- |
| `RPMLINT_OPTIONS | `--info` | options to pass to `rpmlint` |
| `RPMS` | `*.rpm` | input files for `rpmlint` |

## Debian

The Debian template files `packaging/debian.yml` can be included via

```yaml
include:
  - project: computing/gitlab-ci-templates
    file: packaging/debian.yml
```

This file provides the following job templates

### `.debian:dsc`

This job takes in a `${TARBALL}` and returns a debian source package (`.dsc`) as an artifact.

Example usage:

```yaml
dsc:
  extends:
    - .debian:dsc
  variables:
    TARBALL: myproject.tar.gz
```

Input Variables:

| Name | Default | Purpose |
| ---- | ------- | ------- |
| `DSC_DEPENDENCIES` | `""` (empty) | extra packages to `apt-get install` before building the source packag |
| `TARBALL` | `${CI_PROJECT_NAME}-*.tar.*` | source tarball |

### `.debian:deb`

This job takes in a debian source package and associated files and
returns one or more binary debian packages (`.deb`) as artifacts.

Example usage:

```yaml
deb:
  extends:
    - .debian:deb
  needs:
    - dsc
  variables:
    DSC: "myproject_*.dsc"
```

Input Variables:

| Name | Default | Purpose |
| ---- | ------- | ------- |
| `DSC` | `${CI_PROJECT_NAME}_*.dsc` | debian source package |

### `.debian:lintian`

Runs `lintian` against debian binary packages pulled in from dependencies

Example usage:

```yaml
lintian:
  extends:
    - .debian:lint
  needs:
    - deb
```

Input Variables:

| Name | Default | Purpose |
| ---- | ------- | ------- |
| `LINTIAN_OPTIONS | `--color=auth` | options to pass to `lintian` |
| `LINTIAN_TARGET` | `*.changes` | input files for `lintian` |

# Python templates

## Base

The Python template file `python/base.yml` can be included via

```yaml
include:
  - project: computing/gitlab-ci-templates
    file: python/base.yml
```

### Job templates

This file provides the following job templates:

#### `.python:base`

The base Python job template that just configures local `pip` caching and
a default `image: python`.

Example usage:

```yaml
test:
  extends:
    - .python:base
  before_script:
    - python -m pip install pytest
  script:
    - python -m pytest
```

## Build

The Python template file `python/build.yml` can be included via

```yaml
include:
  - project: computing/gitlab-ci-templates
    file: python/build.yml
```

### Job templates:

This file provides the following job templates:

#### `.python:dist`

This job builds a source distribution (tarball) and a wheel using
[`pypa-build`](https://pypa-build.readthedocs.io/en/stable/) and uploads
those as artifacts.

This job approximately runs the following script:

```bash
python -m pip install build
python -m build --sdist --wheel --outdir .
```

Example usage:

```yaml
sdist:
  extends:
    - .python:dist
```

Input variables:

| Name             | Default  | Purpose                                                          |
| ---------------- | -------- | ---------------------------------------------------------------- |
| `PYTHON`         | `python` | the path of the python interpreter to use                        |
| `BUILD_REQUIRES` | (empty)  | extra packages to `pip install` before running `python -m build` |
| `BUILD_OPTIONS`  | (empty)  | extra options to pass to `python -m build`                       |
| `SRCDIR`         | `"."`    | path of source directory for `python -m build`                   |

## Docs

The Python template file `python/docs.yml` can be included via

```yaml
include:
  - project
    file: python/docs.yml
```

### Job templates

This file provides the following job templates:

#### `.python:mkdocs`

This job runs `mkdocs build`, uploading the output `site/` directory as an artifact.

Example usage (including gitlab `pages` job):

```yaml
mkdocs:
  extends:
    - .python:mkdocs
  variables:
    REQUIREMENTS: "-r requirements.txt"

pages:
  needs: [mkdocs]
  script:
    - mv -v site public
  artifacts:
    paths:
      - public
```

Input variables:

| Name             | Default    | Purpose                                                                                                         |
| ---------------- | ---------- | --------------------------------------------------------------------------------------------------------------- |
| `PYTHON`         | `python`   | the path of the python interpreter to use                                                                       |
| `REQUIREMENTS`   | `"mkdocs"` | other packages to `pip install` before running `mkdocs` (use `"-r <file>"` to install from a requirements file) |
| `MKDOCS_OPTIONS` | (empty)    | extra options to pass to `mkdocs`                                                                               |

#### `.python:sphinx`

This job runs `sphinx`, uploading the HTML output directory `html/` as an artifact.
output b`site/` directory as an artifact.

This job approximately runs the following:

```bash
cd docs
make html BUILDDIR=${CI_PROJECT_DIR}
```

However, if a `docs/Makefile` isn't found, the following is run:

```bash
cd docs
sphinx-build -b html . ${CI_PROJECT_DIR}
```

Example usage (including gitlab `pages` job):

```yaml
sphinx:
  extends:
    - .python:sphinx
  variables:
    REQUIREMENTS: "-r docs/requirements.txt"

pages:
  needs: [mkdocs]
  script:
    - mv -v site public
  artifacts:
    paths:
      - public
```

Input variables:

| Name           | Default    | Purpose                                                                                                         |
| -------------- | ---------- | --------------------------------------------------------------------------------------------------------------- |
| `PYTHON`       | `python`   | the path of the python interpreter to use                                                                       |
| `REQUIREMENTS` | `"mkdocs"` | other packages to `pip install` before running `sphinx` (use `"-r <file>"` to install from a requirements file) |
| `SPHINXOPTS`   | (empty)    | extra options to pass to `sphinx`                                                                               |
| `SOURCEDIR`    | `"docs"`   | directory containing documentation sources                                                                      |

## Lint

The Python template file `python/lint.yml` can be included via

```yaml
include:
  - project: computing/gitlab-ci-templates
    file: python/lint.yml
```

### Job templates

This file provides the following job templates:

#### `.python:flake8`

This job runs the `flake8` linter over the project source, uploading a
code quality report.

Example usage:

```yaml
lint:
  extends:
    - .python:flake8
```

Input variables:

| Name             | Default  | Purpose                                                 |
| ---------------- | -------- | ------------------------------------------------------- |
| `PYTHON`         | `python` | the path of the python interpreter to use               |
| `FLAKE8_PLUGINS` | (empty)  | other packages to `pip install` before running `flake8` |
| `FLAKE8_TARGET`  | `"."`    | the path over which to run `flake8`                     |
| `FLAKE8_OPTIONS` | (empty)  | extra options to pass to `flake8`                       |

## Test

The Python template file `python/test.yml` can be included via

```yaml
include:
  - project: computing/gitlab-ci-templates
    file: python/test.yml
```

### Job templates

This file provides the following job templates:

#### `.python:pytest`

This job installs a project and runs `pytest`, uploading JUnit XML test
and coverage reports.

With default options, this job approximately runs the following script:

```bash
pip install .
pip install pytest
pytest --cov=${CI_PROJECT_NAME}
```

Example usage:

```yaml
test:
  extends:
    - .python:pytest
```

Input variables

| Name              | Default                   | Purpose                                                   |
| ----------------- | ------------------------- | --------------------------------------------------------- |
| `PYTHON`          | `python`                  | the path of the python interpreter to use                 |
| `PIP_OPTIONS`     | (empty)                   | options to pass to `pip install`                          |
| `TESTS_REQUIRE`   | (empty)                   | extra packages to `pip install` before running `pytest`   |
| `INSTALL_TARGET`  | `"."`                     | the path to pass to `pip install` to install the project  |
| `PYTEST_OPTIONS`  | (empty)                   | extra options to pass to pytest                           |
| `COVERAGE_TARGET` | `${CI_PROJECT_NAME//-/_}` | the path to pass to `pytest --cov={}` to measure coverage |

Here's another example of configure this job to install from a tarball
then runs tests using `pytest-xdist`, with coverage measured from the
installed library:

```yaml
test:
  extends:
    - .python:pytest
  variables:
    GIT_STRATEGY: none
    TESTS_REQUIRE: "pytest-xdist"
    INSTALL_TARGET: "myproject-*.tar.*"
    COVERAGE_TARGET: "my_library"
    PYTEST_OPTIONS: "--pyargs my_library"
```

In this instance the job effectively runs this script:

```bash
pip install myproject-*.tar.*
pip install pytest pytest-xdist
pytest --cov=my_library --pyargs my_library
```



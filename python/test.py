# -*- coding: utf-8 -*-

"""Example python code with deliberate quality issues

Used to test the flake8 gitlab-ci template
"""

__author__ = "Duncan Macleod <duncan.macleod@ligo.org>"

def my_function(a):
    return a ** 2

print(my_function( 4 ))
